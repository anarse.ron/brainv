import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';
import {FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ThrowStmt } from '@angular/compiler';
import { HotofferPage } from '../home/hotoffer/hotoffer.page';
import { NavController } from '@ionic/angular';
import { ToastController } from '@ionic/angular';
import { HometabsPage } from '../home/hometabs/hometabs.page';
import { AuthService } from '../auth/auth.service';
import { AngularFireAuth } from 'angularfire2/auth';
import { JsonPipe } from '@angular/common';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage {

  loginform: FormGroup;
  submitted = false;
  regformvalue;
  rootPage;

  constructor(public navCtrl : NavController,private storage:Storage,private router: Router,public fAuth:AngularFireAuth, public toastController:ToastController) { 
  }

  regform

  ngOnInit() {
    this.loginform = new FormGroup({
      email: new FormControl(null, [Validators.required]),
      password : new FormControl(null, [Validators.required])
    });

  
    this.storage.get('user').then((val) => {
      this.regformvalue = val;
    });

    

  }

  get f() { return this.loginform.controls; }

  
  async errorToast() {
    const toast = await this.toastController.create({
      message: 'Please register first or check entered credentials',
      duration: 2000,
    });
    toast.present();
  }
  async loginToast() {
    const toast = await this.toastController.create({
      message: 'Login Successfull',
      duration: 2000,
    });
    toast.present();
  }


    async login() {

      this.submitted = true;

      try {
        var r = await this.fAuth.auth.signInWithEmailAndPassword(
        this.loginform.value.email,
        this.loginform.value.password
        ).then((resp)=>{
          console.log(resp)
           this.regform = resp;
           this.storage.set('user', JSON.stringify(this.regform));
          this.router.navigateByUrl('home/tabs/hometabs');
        })
  
      } catch (err) {
        this.errorToast()
        console.error(err + '--error');
      }
    }
}
