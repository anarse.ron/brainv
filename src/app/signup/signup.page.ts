import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';
import {FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AngularFireAuth } from 'angularfire2/auth';
import { NavController, ToastController } from '@ionic/angular';


@Component({
  selector: 'app-signup',
  templateUrl: './signup.page.html',
  styleUrls: ['./signup.page.scss'],
})

export class SignupPage implements OnInit {

  constructor(public navCtrl : NavController,private storage:Storage,private router: Router,public fAuth:AngularFireAuth,public toastController : ToastController) { 
  }

  signInform: FormGroup;
  submitted = false;

  
  ngOnInit() {

    this.signInform = new FormGroup({
      userName: new FormControl(null, [Validators.required]),
      email: new FormControl(null, [Validators.required]),
      mobile: new FormControl(null, [Validators.required]),
      password: new FormControl(null, [Validators.required])
    });  

  }
  get f() { return this.signInform.controls; }

  async errorToast() {
    const toast = await this.toastController.create({
      message: 'Please register first or check entered credentials',
      duration: 2000,
    });
    toast.present();
  }
  async loginToast() {
    const toast = await this.toastController.create({
      message: 'Registration Successfull',
      duration: 2000,
    });
    toast.present();
  }
  

  async register() {
        this.submitted = true;

    try {
      var r = await this.fAuth.auth.createUserWithEmailAndPassword(
        this.signInform.value.email,
        this.signInform.value.password,
      ).then(res => {
        console.log(res)

          

      })
        console.log("Successfully registered!");
        this.loginToast()
        this.navCtrl.navigateRoot('login');
    } catch (err) {
      console.error(err + '--------');
    }
  }

}
