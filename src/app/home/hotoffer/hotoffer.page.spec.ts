import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { HotofferPage } from './hotoffer.page';

describe('HotofferPage', () => {
  let component: HotofferPage;
  let fixture: ComponentFixture<HotofferPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HotofferPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(HotofferPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
