import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomePage } from './home.page';
import { AuthGuard } from '../auth/auth.guard';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full',
  },

  {
    path: 'tabs',
    component: HomePage,
    children: [
      {
        path: 'hometabs',
        children: [
          {
            path: '',
            loadChildren: () => import('./hometabs/hometabs.module').then(m => m.HometabsPageModule),
          },
          {
            path: 'offerlisting',
            loadChildren: () => import('./hometabs/offerlisting/offerlisting.module').then(m => m.OfferlistingPageModule)
          },
          {
            path: ':productId',
            loadChildren: () => import('./hometabs/offerdetails/offerdetails.module').then(m => m.OfferdetailsPageModule)
          },
          
        ]

      },
      
      {
        path: 'hotoffer',
        children: [
          {
            path: '',
            loadChildren: () => import('./hotoffer/hotoffer.module').then(m => m.HotofferPageModule),

          },

        ]

      },

      {
        path: 'mycart',
        children: [
          {
            path: '',
            loadChildren: () => import('./mycart/mycart.module').then(m => m.MycartPageModule)

          },
        ]
      },
    ]
  },
 
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HomePageRoutingModule { }
