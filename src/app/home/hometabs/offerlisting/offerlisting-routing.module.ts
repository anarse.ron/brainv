import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { OfferlistingPage } from './offerlisting.page';

const routes: Routes = [
  {
    path: '',
    component: OfferlistingPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class OfferlistingPageRoutingModule {}
