import { Component, OnInit } from '@angular/core';
import { ProductsService } from '../../products.service';


@Component({
  selector: 'app-offerlisting',
  templateUrl: './offerlisting.page.html',
  styleUrls: ['./offerlisting.page.scss'],
})
export class OfferlistingPage implements OnInit {

  constructor(private productServciee : ProductsService) { }
  
  section: string = 'one';

  product=[];

  ngOnInit() {

   this.product =  this.productServciee.getAllRecipes();

    console.log(this.product)
  }

}
