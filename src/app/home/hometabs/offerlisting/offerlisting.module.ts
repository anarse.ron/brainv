import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { OfferlistingPageRoutingModule } from './offerlisting-routing.module';

import { OfferlistingPage } from './offerlisting.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    OfferlistingPageRoutingModule
  ],
  declarations: [OfferlistingPage]
})
export class OfferlistingPageModule {}
