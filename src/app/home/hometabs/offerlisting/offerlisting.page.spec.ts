import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { OfferlistingPage } from './offerlisting.page';

describe('OfferlistingPage', () => {
  let component: OfferlistingPage;
  let fixture: ComponentFixture<OfferlistingPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OfferlistingPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(OfferlistingPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
