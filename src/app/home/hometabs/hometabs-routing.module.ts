import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HometabsPage } from './hometabs.page';

const routes: Routes = [
  {
    path: '',
    component: HometabsPage
  },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HometabsPageRoutingModule {}
