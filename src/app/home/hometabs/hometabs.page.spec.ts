import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { HometabsPage } from './hometabs.page';

describe('HometabsPage', () => {
  let component: HometabsPage;
  let fixture: ComponentFixture<HometabsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HometabsPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(HometabsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
